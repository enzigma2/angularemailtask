import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';
import { HookPracticalComponent } from './hook-practical/hook-practical.component';
import { EmailTaskComponent } from './email-task/email-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FirstPipePipe } from './first-pipe.pipe';
import { StandardpipeComponent } from './standardpipe/standardpipe.component';
import { CrudService } from './crud.service';
import { AnimalRouterComponent } from './animal-router/animal-router.component';
import { HomeCompForRouterComponent } from './home-comp-for-router/home-comp-for-router.component';
import { DisplayChildComponent } from './display-child/display-child.component';
import { EmailService } from './email.service';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    HookPracticalComponent,
    EmailTaskComponent,
    FirstPipePipe,
    StandardpipeComponent,
    AnimalRouterComponent,
    HomeCompForRouterComponent,
    DisplayChildComponent,
  ],
  imports: [BrowserModule, FormsModule, AppRoutingModule, ReactiveFormsModule],
  providers: [CrudService, EmailService],
  bootstrap: [AppComponent],
})
export class AppModule {}
