import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstPipe'
})
export class FirstPipePipe implements PipeTransform 
{
   transform(value: number) 
   { 
    return Math.sqrt(value);  
   }
}  

  // transform(base: number, exponent: number): number 
  // {
  //   return Math.pow(base, exponent);
  // }

