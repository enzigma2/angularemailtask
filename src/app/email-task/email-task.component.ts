import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Email } from '../email.model';
import { EmailService } from '../email.service';
// import { EmailService } from '../service-email.service';

@Component({
  selector: 'app-email-task',
  templateUrl: './email-task.component.html',
  styleUrls: ['./email-task.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class EmailTaskComponent implements OnInit {
  isShow = true;
  registerForm: any;
  bodylength: any;
  submitted = false;
  Comments = 'heyy';
  user: Email;
  userList: Array<Email>;
  userListOp: Array<Email>;

  constructor(private formBuilder: FormBuilder, public serdata: EmailService) {
    this.registerForm = FormGroup;
    this.user = new Email('', '', '', '');
    this.userList = new Array();
    this.userListOp = JSON.parse(localStorage.getItem('users')!);
    console.log('the userlistis', this.userListOp);
    // console.log('length is',this.bodylength);
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }
  // toggleDisplay1() {
  //   // this.isShow = !this.isShow;
  // }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      body: ['', Validators.required],
      firstName: ['', Validators.required],
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    console.log('i am in function');
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.userList = this.serdata.getEmails();

    this.serdata.createEmail(this.userList, this.user);
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
