import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-standardpipe',
  templateUrl: './standardpipe.component.html',
  styleUrls: ['./standardpipe.component.scss']
})
export class StandardpipeComponent implements OnInit {

  constructor() {

    
   }
   title = 'my-first-app';  
   todaydate = new Date();  
   jsonval = {name: 'Alex', age: '25', address:{a1: 'Paris', a2: 'France'}};  
   months = ['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun',  
     'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];  
 
  ngOnInit(): void {
  }

}
