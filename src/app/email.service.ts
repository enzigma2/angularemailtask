import { Injectable } from '@angular/core';
import { Email } from './email.model';

// import { Injectable } from '@angular/core';
// import { Email } from './email.model';

export class EmailService {
  constructor() {}

  //createEmail(userList: Array<Email>, user: Email) : boolean

  // createEmail(userList: Array<Email>, user: Email): boolean {
  //   try {
  //     if (!Array.isArray(userList)) {
  //       userList = [];
  //     }
  //     userList.push(user);
  //     localStorage.setItem('users', JSON.stringify(userList));
  //   } catch (error) {
  //     return false;
  //   }
  //   return true;
  // }

  createEmail(userList: Array<Email>, user: Email) {
    if (!Array.isArray(userList)) {
      userList = [];
    }

    userList.push(user);
    localStorage.setItem('users', JSON.stringify(userList));
    //  userListOp = userList;
  }

  getEmails(): Email[] {
    return JSON.parse(localStorage.getItem('users') || '{}');
  }
}
