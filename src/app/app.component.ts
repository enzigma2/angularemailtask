import {Component, SimpleChanges , OnInit, DoCheck, OnChanges , AfterContentInit,AfterContentChecked,AfterViewInit,AfterViewChecked, ViewEncapsulation, ChangeDetectionStrategy} from '@angular/core';
import { CrudService } from './crud.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
//  template:`<app-server></app-server>
 // <app-server></app-server>`,
  styleUrls: ['./app.component.scss'],
 // styles : [`h1
  //{
  //color : yellow}`]
  // encapsulation: ViewEncapsulation.None,
  providers: [CrudService]
 
  
})
export class AppComponent implements OnInit,DoCheck,OnChanges,AfterContentInit,AfterContentChecked,AfterViewInit,AfterViewChecked
{

  
  items = ['item1', 'item2', 'item3', 'item4'] as any;
  title = 'projectangular';
  cources= [1,2];

  currentValue='aarti';

  
  usercreated= false; 
  users=['aarti','Chutki'];
  username='firstname';
  userCreatedStatus: string | undefined;
  power = 5;
  factor = 1;
  // sum: number;
  varmessage: any;
  data: any;


/////////////////////////////////////////////
// Child to parent and vice versa
  usernameEnter='EnterName';
  collectuser='';
  nameadded=false;
  result='';
  finalresult: any;
  output: any;

 sendName(event : Event)
  {
    this.usernameEnter =(<HTMLInputElement> event.target).value;
  }
  addname()
{
  this.nameadded=true;
  this.collectuser=this.usernameEnter;
  this.result='Hello'+this.collectuser;
  this.finalresult=this.usernameEnter;
}

// acceptData(data: any)
// {
//   console.log('data is'+data);
// }


////////////////////////////////////////////////



  OnUpdateServerName(event:any)
  {
    if(event.target.value !== null)
    this.username = event.target.value;
  }

  OnCreateUser()
  { 
    this.usercreated=true;
    this.users.push(this.username); 
    this.userCreatedStatus=' was created and the name is'+this.username;
  }

  
  constructor(ser:CrudService) 
  {
    console.log('constructor is called');
    // this.sum = ser.add(1,2,3,4);
   }
  
   ngOnInit()
  { 

  

    console.log('ng OnInit is called');
    // this.varmessage =   {  firstname: 'Sahosoft'}
 
    //  console.log('ng onit is called');
    //  this.data = new BehaviorSubject(this.varmessage);

  }
 
  // changeName() 
  // {
  //   this.varmessage = {firstname: 'Sahosoft Angular' }
  // }

  ngDoCheck()
  {

    console.log('ng Dockeck is called');
  }
 
  ngOnChanges(element: SimpleChanges)
  {
 
    console.log('onchanges is called');    
    console.log('element');
    

  }

  ngAfterContentInit()
  {
    
    console.log('ng aftercontent init is called');
  }
  ngAfterContentChecked()
  {
    console.log('ng ngAfterContentChecked is called');
  }

 ngAfterViewInit()
 {

  console.log('ng ngAfterViewInit is called');
 }
ngAfterViewChecked()
{
  console.log('ng ngAfterViewChecked is called');
}
}



