import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimalRouterComponent } from './animal-router/animal-router.component';
import { HomeCompForRouterComponent } from './home-comp-for-router/home-comp-for-router.component';

const routes: Routes = [
{ path:'', component: HomeCompForRouterComponent },
{ path:'animalRouter', component: AnimalRouterComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
