import { Component, OnInit, SimpleChanges, Input } from '@angular/core';

@Component({
   selector: 'app-server',
   templateUrl: './server.component.html', 
   styles: [`.online
   {
            color: white;
   }`]
   
})

export class ServerComponent  {

  ServerIds: number = 10;
  ServerStatus: String = "offline";
  changeLog: any;


  // @Input() hero: 'Hero' | undefined;
  // @Input() power: string | undefined;
     @Input() name: string | undefined;

  constructor()
  {
    this.ServerStatus=Math.random() > 0.5 ? 'online' : 'offline';
  }
  getcolor()
  {
    return this.ServerStatus === 'online' ? 'green' : 'red';
  }


  // ngOnChanges(changes: SimpleChanges) {
  //   for (const propName in changes) {
  //     const chng = changes[propName];
  //     const cur  = JSON.stringify(chng.currentValue);
  //     const prev = JSON.stringify(chng.previousValue);
  //     this.changeLog.push(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
  //   }
  // }

}