export class Email {
  email: string | undefined;
  body: string;
  firstName: string | undefined;
  bodylength: any;

  constructor(email: string, body: string, firstName: string, bodylength: any) {
    this.email = email;
    this.body = body;
    this.firstName = firstName;
    this.bodylength = bodylength;
  }
}
