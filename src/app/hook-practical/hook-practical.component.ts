import { Component, OnInit, Input, SimpleChanges, ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-hook-practical',
  templateUrl: './hook-practical.component.html',
  styleUrls: ['./hook-practical.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush,
 encapsulation: ViewEncapsulation.Emulated,
 
})
export class HookPracticalComponent implements OnInit 
{ 
  
  @Input() user: string = 'ss';
  @Input() company: any;
 
  constructor() 
  {
    
   }
  ngOnInit() {
    console.log('oninit from CHILD is called');
  
  }
  
  ngOnChanges(element: SimpleChanges)
  {
    console.log('this.username===> in changed in child component',this.user);
    console.log('onchanges is called');
    
    console.log('element');
  }

}
