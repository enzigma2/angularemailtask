import { Component, OnInit, Input ,EventEmitter, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss'],
  encapsulation: ViewEncapsulation.None,
  
})
export class ServersComponent implements OnInit {

   AllowNewServer=false;
   servername='test';
   servercreationstatus='no server was created';
   servercreated= false;
   
   servers=['firstserver','Secondserver'];
   showsecret=false;
   log = [] as  any;
   names=['aarti','chutki','monti'] as any;
  //  myText: string = "Hello World";
   myText='';

   
   @Input() item : string | undefined;


  //  @Output() newItemEvent = new EventEmitter<string>();
  //  addNewItem(value: string) 
  // {
  //   this.newItemEvent.emit(value);
  // }

  constructor() {
          setTimeout(()=>{ this.AllowNewServer= true },20)
                }
                

  ngOnInit(): void {
    this.myText='aarti';
       
  }

  OnCreateServer()
  { 
    this.servercreated=true;
    this.servers.push(this.servername); 
    this.servercreationstatus='server was created and the name is'+this.servername;
  }

  OnUpdateServerName(event : Event)
  {
    this.servername =(<HTMLInputElement> event.target).value;
  }

  onToggleDisplay()
  {
    this.showsecret = !this.showsecret;
    this.log.push (this.log.length+1);
  }

}
